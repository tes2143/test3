<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="/test-project/resources/css/ManagerMainMenu.css">
<title>Insert title here</title>
</head>
<body>

<div class="container">

    <aside class="side">
        <div class="profile">
            <div class="img"><img src="/test-project/resources/images/profile.jpg" alt=""></div>
            <div class="set"><img src="/test-project/resources/images/setting.png" alt=""></div>

            <p>나대리</p>
            <span>꿀잠 지원실 / 대리</span><br>
            <a href="#">로그아웃l</a>
        </div>
        <div class="menu">
            <div><a href="#">홈</a></div>
            <div><a href="#">전자결재</a></div>
            <div>
                <a href="#">게시판</a>
            </div>
            <div class="sub-menu">
                <p><a href="#">공유게시판</a></p>
                <p><a href="#">새소식게시판</a></p>
                <p><a href="#">자유게시판</a></p>
                <p><a href="#">질문게시판</a></p>
                <p><a href="#">사내게시판</a></p>
            </div>

            <div><a href="#">일정</a></div>
            <div><a href="#">주소록</a></div>
        </div>
    </aside>

    <section class="sectionBox">

        <div class="sectionMain">
            <div class="section01">

                <h1 class="AAA">2022.03 </h1>
                <h3 class="addDay"><a href="#">회사 일정 추가</a></h3>

                <div class="icon">
                    <i class="fa-solid fa-angle-left"></i>
                    <i class="fa-solid fa-angle-right"></i>
                </div>

            </div>


            <div class="section02">
                <table id="table1"border="1">
                    <thead>
                    <tr>
                        <td class="closedDay">Sunday</td><td>Monday</td><td>Tuesday</td><td>Wednesday</td><td>Thursday</td><td>Friday</td><td>Saturday</td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="si">27</td><td class="si">28</td><td><span class="closedDay">1 삼일절</span></td><td>2</td><td>3</td><td>4</td><td>5</td>
                    </tr>
                    <tr>
                        <td>6</td><td>7</td><td>8</td><td class="closedDay">9 대통령 선거일</td><td>10</td><td>11</td><td>12</td>
                    </tr>
                    <tr>
                        <td>13</td><td>14</td><td>15</td><td id="c">16</td><td>17</td><td>18</td><td>19</td>
                    </tr>
                    <tr>
                        <td>20</td><td>21</td><td>22</td><td>23</td><td>24</td><td><span>25</span></td><td>26</td>
                    </tr>
                    <tr>
                        <td>27</td><td>28</td><td>29</td><td>30</td><td>31</td><td class="si">1</td><td class="si">2</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </section>

</div>



</body>
</html>